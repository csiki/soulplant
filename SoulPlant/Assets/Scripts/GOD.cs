﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GOD : MonoBehaviour
{
	[SerializeField]
	Transform _tree;
    TreeAnimation treeAnimation;
	bool _moveCamera = false;
	Vector3 _cameraPositionTarget = new Vector3(-0.02f, 1.36f, -2.2f);
	Vector3 _cameraRotationTarget = new Vector3(4, 0, 0);
	Camera _mainCamera;
	[SerializeField]
	Light _sun;
	int _treeStage;
    public float daylightBaseline = 1f;
    public float maxDayLight = 3f;
    public bool dayLight = false;
    public float lightChangeSpeed = 0.04f;
    public bool cameraShouldMove = true;
    public const float treeStageShiftThreshold = 7f; // TODO finegrain
    private float stageLeveling = 0f;

    public float neutralFeatureValue = 0.5f;
    public float featureThreshold = 0.55f;
    private float lastMeditation = 0;
    private float lastConcentration = 0;

    private float lastARGrowth = 0;
    private bool ARCanGrow = false;

    private List<EEGDevice> devices = new List<EEGDevice>();
    private EEGDevice connectedDevice = null;
    private bool alreadyConnected = false;
    public EEGDevice ConnectedDevice
    {
        get { return connectedDevice; }
    }
    public bool AlreadyConnected
    {
        get { return alreadyConnected; }
    }

    // Use this for initialization
    void Start ()
	{
        treeAnimation = _tree.GetComponent<TreeAnimation>();
		_mainCamera = Camera.main;

        // Muse connection
        Refresh();
        Connect();
    }
	
	// Update is called once per frame
	void Update ()
	{
        if (Application.loadedLevelName == "ARPlant") // AR scene
        {
            if (lastARGrowth + 0.3f < Time.time)
            {
                lastARGrowth = Time.time;
                treeAnimation.NextStage();
            }
            return;
        }

        if (alreadyConnected && !connectedDevice.IsConnected())
            alreadyConnected = false;

        if (!alreadyConnected && (devices == null || devices.Count == 0))
        {
            Refresh();
            Connect();
        }

        if (!alreadyConnected)
        {
            foreach (var dev in devices)
            {
                //Debug.Log("isconnected " + dev.IsConnected().ToString() + "dev: " + dev);
                if (dev.IsConnected()) // EEG connect
                {
                    alreadyConnected = true;
                    connectedDevice = dev;
                    devices.Clear();
                    dev.ActivateFeature("MuseMellowFeature");
                    dev.ActivateFeature("MuseConcentrationFeature");
                    //dev.ActivateFeature("MeditationFeature");
                    //Debug.Log("MUSE CONNECTED: " + connectedDevice.GetVersion());
                    break;
                }
            }
        }

        if (!_mainCamera) _mainCamera = Camera.main;
        if (_moveCamera && cameraShouldMove) {
			_mainCamera.transform.position = Vector3.Lerp (_mainCamera.transform.position, _cameraPositionTarget, 0.03f);
			float distance = Vector3.Distance (_mainCamera.transform.position, _cameraPositionTarget);
			if (distance < 0.01) {
				_mainCamera.transform.position = _cameraPositionTarget;
				//_moveCamera = false;
			}

			//Vector3.RotateTowards (Camera.main.transform.position, _cameraPositionTarget, 0.2f, 0.2f);
		}

        // meditation // TODO change tree albido
		if (IsMeditating ()) {
			if (maxDayLight - _sun.intensity < 0.04)
				_sun.intensity = maxDayLight;
			else
				_sun.intensity = Mathf.Lerp (_sun.intensity, maxDayLight, lightChangeSpeed);
		}
        else {
            float towards = /*dayLight ? daylightBaseline :*/ 0;
            if (_sun.intensity - towards < 0.04)
                _sun.intensity = towards;
            else
                _sun.intensity = Mathf.Lerp(_sun.intensity, towards, lightChangeSpeed);
		}

        // focus
        if (IsFocusing())
        {
            //Debug.Log("Stage lvl: " + stageLeveling);
            stageLeveling += (float)connectedDevice["MuseConcentrationFeature"].Value - featureThreshold;
            if (stageLeveling > treeStageShiftThreshold)
            {
                stageLeveling = 0f;
                treeAnimation.NextStage();
            }
        }
    }

    public void pullCamera()
    {
        _cameraPositionTarget = _mainCamera.transform.position + new Vector3(0, 0, -0.16f);
    }

	private bool IsMeditating () // TODO interpolate .5
	{
//#if UNITY_EDITOR
//            return Input.GetKey (KeyCode.Space);
//#else
        // meditation acquisition
        if (connectedDevice != null && connectedDevice.IsConnected())
        {
            if (connectedDevice.SignalQuality(EEGChannel.Fp1) < 3 && connectedDevice.SignalQuality(EEGChannel.Fp2) < 3) // maxiking Fp1, Fp2 contact (1+1)
            {
                dayLight = true;
                //Debug.Log("Meditation: " + connectedDevice["MuseMellowFeature"].Value);
                if (lastMeditation == connectedDevice["MuseMellowFeature"].Value)
                    lastMeditation = (float) (connectedDevice["MuseMellowFeature"].Value * 20.0 + neutralFeatureValue) / 21f;
                else
                    lastMeditation = (float) connectedDevice["MuseMellowFeature"].Value;
                if (lastMeditation > featureThreshold)
                    return true;
            }
            else
            {
                dayLight = false;
                //Debug.Log("SEC FP1: " + connectedDevice.SignalQuality(EEGChannel.Fp1) + "; SEC FP2: " + connectedDevice.SignalQuality(EEGChannel.Fp2));
            }
        }
        return false;
//#endif
    }

    private bool IsFocusing()
    {
//#if UNITY_EDITOR
        /*if (Input.GetKey(KeyCode.F))
            treeAnimation.NextStage();
        return false;*/
//#else
        if (connectedDevice != null && connectedDevice.IsConnected())
        {
            if (connectedDevice.SignalQuality(EEGChannel.Fp1) < 3 && connectedDevice.SignalQuality(EEGChannel.Fp2) < 3) // maxiking Fp1, Fp2 contact (1 + 1)
            {
                dayLight = true;
                //Debug.Log("Concentration: " + connectedDevice["MuseConcentrationFeature"].Value);
                if (lastConcentration == connectedDevice["MuseConcentrationFeature"].Value)
                    lastConcentration = (float)(connectedDevice["MuseConcentrationFeature"].Value * 20.0 + neutralFeatureValue) / 21f;
                else
                    lastConcentration = (float)connectedDevice["MuseConcentrationFeature"].Value;
                if (lastConcentration > featureThreshold)
                    return true;
            }
            else
            {
                dayLight = false;
            }
        }
        return false;
//#endif
    }

    public void SeedWasPutOnPot ()
	{
		_tree.gameObject.SetActive (true);
		_moveCamera = true;
        ARCanGrow = true;

        // make seeds disappear
        GameObject[] seeds = GameObject.FindGameObjectsWithTag("Seed");
        for (int i = 0; i < seeds.Length; i++)
            seeds[i].SetActive(false);
    }

    public void Refresh()
    {
#if !UNITY_EDITOR
        devices = EEGDeviceFamily.LookForAllKindsOfDevices();
        if (devices.Count > 0)
        {
            foreach (var dev in devices)
                Debug.Log("Device found: " + dev.ID);
        }
        else
            Debug.Log("Cannot find device!");
#endif
    }

    public void Connect()
    {
        if (devices.Count > 0)
        {
            Debug.Log("Tries to connect to Muse!");
            foreach (var dev in devices)
            {
                Debug.Log("Device found: " + dev.ID);
                dev.Connect();
                //break; // connect to first one only
            }
        }
        else
            Debug.Log("Cannot connect to device!");
    }

    public void Quit()
    {
        if (connectedDevice != null && connectedDevice.IsConnected())
            connectedDevice.Disconnect();
        Application.Quit();
    }

    public void ChangeToARScene()
    {
        Application.LoadLevel(1);
        if (connectedDevice != null && connectedDevice.IsConnected())
            connectedDevice.Disconnect();
    }
}
