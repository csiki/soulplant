﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SignalIndicator : MonoBehaviour {

    public Sprite inactive;
    public Sprite active;
    public Sprite fit;
    public GameObject godGO;

    private Image image;
    private GOD god;

    void Start () {
        image = gameObject.GetComponent<Image>();
        god = godGO.GetComponent<GOD>();
	}
	
	void Update () {
        if (god.AlreadyConnected)
        {
            Debug.Log("active");
            var tp9SQ = god.ConnectedDevice.SignalQuality(EEGChannel.TP9);
            var tp10SQ = god.ConnectedDevice.SignalQuality(EEGChannel.TP10);
            var fp1SQ = god.ConnectedDevice.SignalQuality(EEGChannel.Fp1);
            var fp2SQ = god.ConnectedDevice.SignalQuality(EEGChannel.Fp2);

            if (fp1SQ == 1 && fp2SQ == 1 && tp9SQ <= 2 && tp10SQ <= 2)
                image.sprite = fit;
            else
                image.sprite = active;
        }
        else
            image.sprite = inactive;
	}
}
