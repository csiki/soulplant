﻿using UnityEngine;
using System.Collections;

public class seedScript : MonoBehaviour
{
	Vector3 _initialScale;
	int _frame;
	Vector3 _screenPoint;
	Vector3 _offset;
	Vector3 _scanPos;
	Vector3 _initPos;
	Transform _pot;
    private bool _isPressing;
    [SerializeField]
	GOD _god;
	// Use this for initialization
	void Start ()
	{

		_pot = GameObject.FindGameObjectWithTag ("Pot").transform;
		_initialScale = transform.localScale;
		_initPos = transform.position;
		/*if (PlayerPrefs.GetInt (STEP, 0) >= 1) {
			GameObject[] seeds = GameObject.FindGameObjectsWithTag ("Seed");
			for (int i =0; i<seeds.Length; i++) {
				seeds [i].SetActive (false);
			}
		}*/
	}
	
	// Update is called once per frame
	void Update ()
	{

		_frame++;
		transform.localScale = _initialScale + Mathf.Sin ((float)_frame / 10) * Vector3.one * 1 / 40;
        if(_isPressing)
            transform.position = Vector3.Lerp(transform.position, _pot.position, 0.15f);



    }
    void OnMouseDown ()
	{

		_screenPoint = Camera.main.WorldToScreenPoint (_scanPos);
        _isPressing = true;
      //  transform.position = Vector3.Lerp(transform.position, _pot.position, 0.1f);
    }
	void OnMouseDrag ()
	{
		Vector3 curScreenPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z);
		
		Vector3 curPosition = Camera.main.ScreenToWorldPoint (curScreenPoint) /*+ offset*/;
		//transform.position = Vector3.Lerp (transform.position, curPosition, 0.1f);

	}
	void OnMouseUp ()
	{
        _isPressing = false;
        if (gameObject.GetComponent<Collider> ().bounds.Intersects (_pot.gameObject.GetComponent<Collider> ().bounds)) {
			_god.SeedWasPutOnPot ();

		} else {
			transform.position = Vector3.Lerp (transform.position, _initPos, 1f);
		}
		Debug.Log (_pot.gameObject.GetComponent<Collider> ().bounds.size);
	}

}
