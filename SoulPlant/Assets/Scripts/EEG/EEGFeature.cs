using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public abstract class EEGFeature {
	
	private EEGDevice device;
	private string name;
    protected bool calibrationReady = true; // by default no calibration
	public string Name {
		get { return name; }
	}
	private float eegHistLenAcquired;
	public float EEGHistSizeAcquired {
		get { return eegHistLenAcquired; }
	}
	private float updatedLastTime = 0;
	private double lastVal = 0;
	protected List<string> dependencies = new List<string>();
	
	protected List<double> valuesSinceLastUpdate = new List<double>();
	public List<double> ValuesSinceLastUpdate {
		get {
			Update();
			return valuesSinceLastUpdate;
		}
	}
	public double Value {
		get {
			Update();
			if (valuesSinceLastUpdate.Count > 0)
				return valuesSinceLastUpdate.Average();
			return lastVal;
		}
	}
	private bool activated = true;
	public bool Activated {
		get { return activated; }
		set { activated = value; }
	}

	public EEGFeature (EEGDevice device, string name, float eegHistLenAcquired) {
		this.device = device;
		this.name = name;
		this.eegHistLenAcquired = eegHistLenAcquired;
		device.IncreaseEEGHistoryLen(eegHistLenAcquired);
	}
	
	public void Update() {
        if (!calibrationReady)
            Calibrate(); // sets calibrationReady
		if (activated && calibrationReady) {
			// checks
			/*if (device.EEGCount == 0 || updatedLastTime == device.EEGLastUpdate)
				return;
			foreach (var featureName in dependencies)
				if (device[featureName].ValuesSinceLastUpdate.Count == 0)
					return;*/
			
			// update
			if (valuesSinceLastUpdate.Count > 0)
				lastVal = valuesSinceLastUpdate.Last();
			updatedLastTime = device.EEGLastUpdate;
			ForceUpdate();
		}
	}

    public virtual void Calibrate() {} // sets calibrationReady (by default there's no calibration)
    protected abstract void ForceUpdate();

    // tools
    public static double AverageOfUncertainlyValidValues(double a, double b)
    {
        double sum = 0;
        int count = 0;
        if (!double.IsNaN(a)) { ++count; sum += a; }
        if (!double.IsNaN(b)) { ++count; sum += b; }
        if (count > 0)
            return sum / count;
        return -1;
    }

    public static double StdDev(IEnumerable<double> values)
    {
        // ref: http://warrenseen.com/blog/2006/03/13/how-to-calculate-standard-deviation/
        double mean = 0.0;
        double sum = 0.0;
        double stdDev = 0.0;
        int n = 0;
        foreach (double val in values)
        {
            n++;
            double delta = val - mean;
            mean += delta / n;
            sum += delta * (val - mean);
        }
        if (1 < n)
            stdDev = Math.Sqrt(sum / (n - 1));

        return stdDev;
    }
}

