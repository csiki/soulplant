﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

// frontal alpha power increase
// frontal theta power increase
public class MeditationFeature : EEGFeature
{
    private int museID = 0;
    private AndroidJavaObject deviceHandler = null;
    private EEGChannel leftChan, rightChan;
    private double absoluteMeditation;
    private double alphaMean = 0, thetaMean = 0;
    private double alphaStd = 0, thetaStd = 0;
    private List<double> alphaPowers = new List<double>();
    private List<double> thetaPowers = new List<double>();
    private float calibrationStarted = 0;
    private float calibrationTime = 40; // in sec

    public MeditationFeature(EEGDevice device, AndroidJavaObject deviceHandler, int museID, EEGChannel leftChan, EEGChannel rightChan) : base(device, "MeditationFeature", 0f)
    {
        //if (!device.Family.IsChannelAvailable(leftChan) || !device.Family.IsChannelAvailable(rightChan))
        //    throw new ArgumentException("MeditationFeature only works with a device with proper frontal channels!");
        this.deviceHandler = deviceHandler;
        this.museID = museID;
        this.leftChan = leftChan;
        this.rightChan = rightChan;
        calibrationReady = false;
    }

    public override void Calibrate()
    {
        calibrationReady = false;
        if (calibrationStarted == 0)
        {
            calibrationStarted = Time.time;
            Debug.Log("MEDITATION CALIBRATION STARTED");
        }
        if (calibrationStarted + calibrationTime < Time.time) {
            alphaMean = alphaPowers.Average();
            alphaStd = StdDev(alphaPowers);
            thetaMean = alphaPowers.Average();
            thetaStd = StdDev(thetaPowers);
            alphaPowers.Clear();
            thetaPowers.Clear();
            calibrationReady = true;
            Debug.Log("MEDITATION CALIBRATION OVER");
            Debug.Log("alpha mean: " + alphaMean + "; alpha std: " + alphaStd + "; theta mean: " + thetaMean + "; theta std: " + thetaStd);
            return;
        }

        // alpha calibration
        var packet = deviceHandler.Call<AndroidJavaObject>("pollAlphaAbsolute", museID);
        if (packet.Get<bool>("valid")) {
            var fp1 = packet.Get<double>("FP1");
            var fp2 = packet.Get<double>("FP2");
            alphaPowers.Add(AverageOfUncertainlyValidValues(fp1, fp2));
        }

        // theta calibration
        packet = deviceHandler.Call<AndroidJavaObject>("pollThetaAbsolute", museID);
        if (packet.Get<bool>("valid"))
        {
            var fp1 = packet.Get<double>("FP1");
            var fp2 = packet.Get<double>("FP2");
            thetaPowers.Add(AverageOfUncertainlyValidValues(fp1, fp2));
        }
    }

    protected override void ForceUpdate()
    {
        valuesSinceLastUpdate.Clear();
        //if (deviceHandler.Call<bool>("blinked", museID)) return;
        var alphaPacket = deviceHandler.Call<AndroidJavaObject>("pollAlphaAbsolute", museID);
        var thetaPacket = deviceHandler.Call<AndroidJavaObject>("pollThetaAbsolute", museID);
        
        var alpha = AverageOfUncertainlyValidValues(alphaPacket.Get<double>("FP1"), alphaPacket.Get<double>("FP2"));
        var theta = AverageOfUncertainlyValidValues(thetaPacket.Get<double>("FP1"), thetaPacket.Get<double>("FP2"));

        if (alpha != -1 && theta != -1)
        {
            double meditation = ((alpha - alphaMean) / alphaStd + (theta - thetaMean) / thetaStd) / 2.0;
            //meditation = Math.Max(Math.Min(meditation, 1), 0); // [0, 1]
            valuesSinceLastUpdate.Add(meditation);
        }
    }
}

