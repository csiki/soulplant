using UnityEngine;
using System.Collections.Generic;
using System;

public class MuseDevice : EEGDevice {
	
	private int museID = 0;
	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject deviceHandler = null;
	private bool connected = false;
    private float signalQualityLastUpdate = 0;
    private Dictionary<EEGChannel, int> signalQuality = new Dictionary<EEGChannel, int>();
	
	public void Init(EEGDeviceFamily family, float eegHistoryLen, int museID, AndroidJavaClass deviceClass, AndroidJavaObject deviceHandler) {
		InitDevice(family.Name + museID, family, eegHistoryLen);
		this.museID = museID;
		this.deviceClass = deviceClass;
		this.deviceHandler = deviceHandler;
		
		// TODO add muse specific features
		//RegisterFeature(new MuseFrontalAlphaRelativeFeature(this, deviceHandler, museID), false);
        RegisterFeature(new MuseMellowFeature(this, deviceHandler, museID), true);
        RegisterFeature(new MuseConcentrationFeature(this, deviceHandler, museID), true);
        //RegisterFeature(new MeditationFeature(this, deviceHandler, museID, EEGChannel.Fp1, EEGChannel.Fp2), false);
    }
	
	// CONNECTION
	public override bool Connect() { // just tries to connect
		if (deviceHandler != null) {
			bool isthereany = deviceHandler.Call<bool>("refresh");
			if (isthereany && deviceHandler.Call<bool>("connect", museID)) {
				connected = IsConnected();
				return true;
			}
		}
		return false;
	}
	public override void Disconnect() {
		deviceHandler.Call("disconnect", museID);
		connected = false;
	}
	public override bool IsConnected() {
        try {
            return deviceHandler.Call<bool>("isConnected", museID);
        }
        catch { return false; }
	}
	public override float SignalQuality (EEGChannel channel) // horseshoe value
	{
		if (!family.IsChannelAvailable(channel))
            throw new ArgumentException(channel.ToString());
        if (signalQualityLastUpdate < Time.time) {
            signalQualityLastUpdate = Time.time;
            var packet = deviceHandler.Call<AndroidJavaObject>("horseshoe", museID);
            if (packet != null) {
                signalQuality.Clear();
                Debug.Log("FP1: " + packet.Get<double>("FP1") + "; FP2: " + packet.Get<double>("FP2"));
                signalQuality.Add(EEGChannel.TP9, (int)packet.Get<double>("TP9"));
                signalQuality.Add(EEGChannel.TP10, (int)packet.Get<double>("TP10"));
                signalQuality.Add(EEGChannel.Fp1, (int)packet.Get<double>("FP1"));
                signalQuality.Add(EEGChannel.Fp2, (int)packet.Get<double>("FP2"));
            }
        }
        if (!signalQuality.ContainsKey(channel))
            return 0; // TODO throw exception
        return signalQuality[channel];
	}
    public override string GetVersion() {
        return deviceHandler.Call<string>("getVersion", museID);
    }

    // RETREIVER METHODS
    protected override void RetreiveEEG() {
		List<AndroidJavaObject> packets = new List<AndroidJavaObject>();
		var eegPacket = deviceHandler.Call<AndroidJavaObject>("pollEEG", museID);
		if (eegPacket != null) {
			while (eegPacket.Get<bool> ("valid")) {
				packets.Add(eegPacket);
				eegPacket = deviceHandler.Call<AndroidJavaObject>("pollEEG", museID);
			}
			foreach (var packet in packets) {
				var dict = new Dictionary<EEGChannel, double>();
				dict.Add(EEGChannel.TP9, packet.Get<double>("TP9"));
				dict.Add(EEGChannel.TP10, packet.Get<double>("TP10"));
				dict.Add(EEGChannel.Fp1, packet.Get<double>("FP1"));
				dict.Add(EEGChannel.Fp2, packet.Get<double>("FP2"));
				eeg.AddLast(new KeyValuePair<float, Dictionary<EEGChannel, double>>(packet.Get<long>("timestamp") / 1000f, dict)); // converted to milliseconds
			}
		}
	}
	protected override void RetreiveAcceleration() {
		List<AndroidJavaObject> packets = new List<AndroidJavaObject>();
		var accPacket = deviceHandler.Call<AndroidJavaObject>("pollAccelerometer", museID);
		while (accPacket != null && accPacket.Get<bool> ("valid")) {
			packets.Add(accPacket);
			accPacket = deviceHandler.Call<AndroidJavaObject>("pollAccelerometer", museID);
		}
		AccelerometerData prev = null;
		if (acc.Count > 0) prev = acc.Last.Value.Value;
		foreach (var packet in packets) {
			AccelerometerData accdata = null;
			if (prev != null)
				accdata = new AccelerometerData(packet.Get<double>("ForBack"), packet.Get<double>("UpDown"), packet.Get<double>("LR"), prev);
			else
				accdata = new AccelerometerData(packet.Get<double>("ForBack"), packet.Get<double>("UpDown"), packet.Get<double>("LR"));
			
			acc.AddLast(new KeyValuePair<float, AccelerometerData>(packet.Get<long>("timestamp") / 1000f, accdata)); // converted to milliseconds
			prev = accdata;
		}
	}
	
	// MONOBEHAVIOR METHODS
	protected override void Start () {}
	protected override void Update() {}
	
}

