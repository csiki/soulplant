﻿using UnityEngine;
using System.Collections.Generic;

public class MuseDeviceUI : MonoBehaviour {

	private List<EEGDevice> devices = new List<EEGDevice>();
    EEGDevice connectedDevice = null;
    private bool alreadyConnected = false;
    private double prevConcentration = -1;
    private double prevMellow = -1;
    private float prevMeditation = -1;
    public GameObject meditationSphere;

	public void Refresh () {
		devices = EEGDeviceFamily.LookForAllKindsOfDevices();
		if (devices.Count > 0) {
			foreach (var dev in devices)
				Debug.Log("Device found: " + dev.ID);
		} else
			Debug.Log("Cannot find device!");
	}

	public void Connect() {
		if (devices.Count > 0) {
			Debug.Log("Tries to connect to Muse!");
            foreach (var dev in devices)
            {
                Debug.Log("Device found: " + dev.ID);
                dev.Connect();
            }
		}
		else
			Debug.Log("Cannot connect to device!");
	}

	public void Quit() {
		if (connectedDevice != null && connectedDevice.IsConnected())
            connectedDevice.Disconnect();
		Application.Quit ();
	}
	
	void Update() {
        foreach (var dev in devices)
        {
            if (dev != null && dev.IsConnected() && !alreadyConnected)
            {
                alreadyConnected = true;
                connectedDevice = dev;
                devices.Clear();
                dev.ActivateFeature("MuseMellowFeature");
                dev.ActivateFeature("MuseConcentrationFeature");
                dev.ActivateFeature("MeditationFeature");
                Debug.Log("MUSE CONNECTED: " + connectedDevice.GetVersion());
                break;
            }
        }
        if (connectedDevice != null && connectedDevice.IsConnected())
        {
            if (connectedDevice["MuseMellowFeature"].Value != prevMellow)
            {
                prevMellow = connectedDevice["MuseMellowFeature"].Value;
                Debug.Log("Mellow: " + prevMellow);
                
            }
            if (connectedDevice["MuseConcentrationFeature"].Value != prevConcentration)
            {
                prevConcentration = connectedDevice["MuseConcentrationFeature"].Value;
                float dummySphereSize = (float)prevConcentration * 60f;
                meditationSphere.transform.localScale = new Vector3(dummySphereSize, dummySphereSize, dummySphereSize);
                Debug.Log("Concentration: " + prevConcentration);
            }
            /*if (prevMeditation != connectedDevice["MeditationFeature"].Value * 60f)
            {
                prevMeditation = (float)connectedDevice["MeditationFeature"].Value * 60f;
                meditationSphere.transform.localScale = new Vector3(prevMeditation, prevMeditation, prevMeditation);
                Debug.Log("Meditation: " + prevMeditation);
            }*/
        }
    }
}
