﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MuseConcentrationFeature : EEGFeature
{
    private int museID = 0;
    private AndroidJavaObject deviceHandler = null;

    public MuseConcentrationFeature(EEGDevice device, AndroidJavaObject deviceHandler, int museID) : base(device, "MuseConcentrationFeature", 0f)
    {
        if (device.Family.Name != "Muse")
            throw new ArgumentException("MuseConcentrationFeature only works with a Muse device!");
        this.deviceHandler = deviceHandler;
        this.museID = museID;
    }

    protected override void ForceUpdate()
    {
        valuesSinceLastUpdate.Clear();
        valuesSinceLastUpdate.Add(deviceHandler.Call<double>("concentration", museID));
    }
}
