﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MuseMellowFeature : EEGFeature
{
    private int museID = 0;
    private AndroidJavaObject deviceHandler = null;

    public MuseMellowFeature(EEGDevice device, AndroidJavaObject deviceHandler, int museID) : base(device, "MuseMellowFeature", 0f)
    {
        if (device.Family.Name != "Muse")
            throw new ArgumentException("MuseMellowFeature only works with a Muse device!");
        this.deviceHandler = deviceHandler;
        this.museID = museID;
    }

    protected override void ForceUpdate()
    {
        //Debug.Log("forceupdate");
        valuesSinceLastUpdate.Clear();
        valuesSinceLastUpdate.Add(deviceHandler.Call<double>("mellow", museID));
    }
}
