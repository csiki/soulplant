﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashDisplay : MonoBehaviour {

    public Texture introSplash;
    public Texture pairMuseSplash;
    public Texture dragSeedSplash;
    public Texture splashBG;
    private Texture[] splashScreens;
    public float timeBetweenSplash;
    private float timeForNextSplash;
    private int splashIndex = 0;
    private bool nextSceneLoading = false;

	void Start () {
        splashScreens = new Texture[3];
        splashScreens[0] = introSplash;
        splashScreens[1] = pairMuseSplash;
        splashScreens[2] = dragSeedSplash;
        timeForNextSplash = Time.time + timeBetweenSplash;
        splashBG.wrapMode = TextureWrapMode.Repeat;
    }
	
	void Update () {}

    public void OnGUI()
    {
        if (Time.time > timeForNextSplash)
        {
            timeForNextSplash = Time.time + timeBetweenSplash;
            ++splashIndex;
        }
        if (splashIndex >= splashScreens.Length)
        {
            if (!nextSceneLoading)
            {
                StartCoroutine(LoadNextScene());
                nextSceneLoading = true;
            }

            // display last splash
            DrawTexturePreservingAspectRatio(splashScreens[splashScreens.Length - 1]);
        }
        else
            DrawTexturePreservingAspectRatio(splashScreens[splashIndex]);
    }

    void DrawTexturePreservingAspectRatio(Texture splash)
    {
        var width = Screen.height * ((float)splash.width / splash.height);
        var leftOffset = (Screen.width - width) / 2f;
        GUI.DrawTextureWithTexCoords(new Rect(0, 0, Screen.width, Screen.height),
            splashBG, new Rect(0, 0, Screen.width / splashBG.width, Screen.height / splashBG.height));
        //GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), splashBG);
        GUI.DrawTexture(new Rect(leftOffset, 0, width, Screen.height), splash);
    }

    IEnumerator LoadNextScene()
    {
        var loading = SceneManager.LoadSceneAsync("MainActivity", LoadSceneMode.Additive);
        loading.allowSceneActivation = false;
        //SceneManager.SetActiveScene(SceneManager.GetActiveScene());
        bool sajt = false;

        while (!loading.isDone && !sajt)
        {
            yield return new WaitForSeconds(2);
            sajt = true;
        }

        loading.allowSceneActivation = true;
        while (!loading.isDone)
        {
            yield return new WaitForSeconds(1);
        }

        SceneManager.SetActiveScene(SceneManager.GetSceneByName("MainActivity"));
        SceneManager.UnloadScene("Splash");
    }
}
