﻿using UnityEngine;
using System.Collections.Generic;

public class TreeAnimation : MonoBehaviour {

    public int stage = 0;
    private List<GameObject> stages = new List<GameObject>();
    public GameObject GODGO;
    private GOD god;

	void Start () {
        for (int i = 1; i <= transform.childCount; ++i)
            stages.Add(transform.FindChild(i.ToString()).gameObject);
        god = GODGO.GetComponent<GOD>();
	}
	
	void Update () {}

    public void NextStage()
    {
        if (stage < stages.Count - 1) {
            stages[stage].SetActive(false);
            ++stage;
            stages[stage].SetActive(true);
            god.pullCamera();
            PlayerPrefs.SetInt("stage", stage);
        }
    }

    public int StageCount()
    {
        return stages.Count;
    }

    public void ChangeAlbedo(float by)
    {
        // TODO
    }
}
